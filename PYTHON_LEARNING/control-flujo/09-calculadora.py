print("Bienvenidos a la calculadora")
print("Para salir escribe salir")
print("Las operaciones son suma, resta, multiplicacion y division")

resultado = ""
while True:
    if not resultado:
        resultado = input("Ingrese numero: ")
        if resultado.lower() == "salir":
            break
        resultado = int(resultado)
    op = input("Ingresa opcion: ")
    if op.lower() == "salir":
        break
    n2 = input("Inserta siguiente numero: ")
    if n2.lower() == "salir":
        break
    n2 = int(n2)

    if op.lower() == "suma":
        resultado += 2
    elif op.lower() == "resta":
        resultado -= 2
    elif op.lower() == "multi":
        resultado *= 2
    elif op.lower() == "div":
        resultado /= 2
    else:
        print("operacion no valida")
        break
    print(f"el resultado es {resultado}")
